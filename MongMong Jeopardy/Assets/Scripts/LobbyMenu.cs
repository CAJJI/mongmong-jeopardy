﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LobbyMenu : MenuBase<LobbyMenu>
{
    public TextMeshProUGUI playerListText;
    public TextMeshProUGUI roomIdText;

    public Toggle allowEarlyBuzzinsToggle;

    public GameObject hostContainer;
    public GameObject playerContainer;

    public override void Open(bool state)
    {
        base.Open(state);
        bool isHost = NetworkManager.isHost;
        hostContainer.SetActive(isHost);
        playerContainer.SetActive(!isHost);        
        UpdateLobby();
    }

    public void UpdateLobby()
    {        
        if (!NetworkManager.isHost)
            return;

        string playerList = "";
        foreach(GameClient client in Lobby.clients.Values)
        {
            playerList += client.username + "\n";
        }
        playerList = playerList.Trim();
        playerListText.text = playerList;

        roomIdText.text = "Room Id: " + Photon.Pun.PhotonNetwork.CurrentRoom.Name.ToUpper();

        allowEarlyBuzzinsToggle.isOn = Lobby.current.allowEarlyBuzzins;
    }

    public void AllowEarlyBuzzins(bool state)
    {
        Lobby.current.allowEarlyBuzzins = state;
    }
    
    public void StartGame()
    {
        Lobby.current.inGame = true;
        Scenes.GoToGame();

        ServerSend.StartGame();
        //ServerSend.GoToScene(Scenes.BUZZER);
    }

    public void Back()
    {
        NetworkManager.LeaveRoom();
    }
}
