﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MenuBase<MainMenu>
{
    string roomId = "";

    public void SetRoomId(string id)
    {
        roomId = id;
    }

    public void HostGame()
    {
        string roomId = GenerateRoomId();
        if (NetworkManager.CreateRoom(roomId))
        {
            MenuController.Instance.CloseAll();
        }
    }

    public void JoinGame()
    {
        if (NetworkManager.JoinRoom(roomId.ToLower()))
        {
            MenuController.Instance.GoToLobbyMenu();
        }
    }

    string GenerateRoomId()
    {
        //string selection = "abcdefghijklmnopqrstuvwxyz";
        string selection = "0123456789";
        string id = "";
        for (int i = 0; i < 3; i++)
        {
            id += selection[Random.Range(0, selection.Length)];
        }
        return id;
    }
}
