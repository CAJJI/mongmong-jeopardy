﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class ServerSend : Singleton<ServerSend>
{
    static PhotonView view { get { if (Instance._photonView == null) Instance._photonView = Instance.GetComponent<PhotonView>(); return Instance._photonView; } }
    PhotonView _photonView;

    public static void RPC(string rpc, RpcTarget rpcTarget, params object[] parameters)
    {
        view.RPC(rpc + "RPC" , rpcTarget, parameters);
    }

    public static void JoinGame(string username, int playerId)
    {
        RPC("JoinGame", RpcTarget.MasterClient, username, playerId);
    }

    [PunRPC]
    public void JoinGameRPC(string username, int playerId)
    {
        Debug.Log("Player has joined: " + username + " : " + playerId);
        Lobby.AddClient(username, playerId);
        SendLobbyInfo(Lobby.GetUsernames(), Lobby.GetPlayerIds());
        SyncClients(Lobby.current.inGame);
    }    

    public static void SendLobbyInfo(string[] usernames, int[] playerIds)
    {
        RPC("SendLobbyInfo", RpcTarget.Others, usernames, playerIds);
    }

    [PunRPC]
    public void SendLobbyInfoRPC(string[] usernames, int[] playerIds)
    {
        LobbyManager.CreateLobby();
        for (int i = 0; i < usernames.Length; i++)
        {
            if (playerIds[i] == GameClient.localPlayerId)
            {
                GameClient.self.playerId = playerIds[i];
                Lobby.AddClient(GameClient.self);
            }
            else
            {
                Lobby.AddClient(usernames[i], playerIds[i]);
            }
        }        
    }

    public static void SyncClients(bool inGame)
    {
        RPC("SyncClients", RpcTarget.Others, inGame);
    }

    [PunRPC]
    public void SyncClientsRPC(bool inGame)
    {
        if (inGame)
        {
            if (Scenes.activeScene != Scenes.BUZZER)
            {
                Scenes.GoToBuzzer();
            }
        }
    }

    public static void GoToScene(string name)
    {
        RPC("GoToScene", RpcTarget.Others, name);
    }

    [PunRPC]
    public void GoToSceneRPC(string name)
    {
        Scenes.Load(name);
    }

    public static void StartGame()
    {
        RPC("StartGame", RpcTarget.Others);
    }

    [PunRPC]
    public void StartGameRPC()
    {
        Lobby.current.inGame = true;
        Scenes.GoToBuzzer();
    }

    public static void BuzzIn()
    {
        RPC("BuzzIn", RpcTarget.MasterClient, GameClient.self.username, GameClient.localPlayerId);
    }

    [PunRPC]
    public void BuzzInRPC(string username, int playerId)
    {
        if (BuzzInController.Exists)
        {
            BuzzInController.Instance.BuzzIn(username, playerId);
        }
    }

    public static void EndGame()
    {
        RPC("EndGame", RpcTarget.All);
    }

    [PunRPC]
    public void EndGameRPC()
    {
        NetworkManager.LeaveRoom();
    }
}
