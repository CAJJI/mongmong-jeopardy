﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

namespace LaundryBear
{
    [InitializeOnLoad]
    public static class PlayFromInitializationScene
    {

        public const string SCENES_STRING = "LB_openScenes";
        public const string ACTIVE_SCENE_PATH = "LB_activeScene";

        private const string USE_PRELAUNCH = "LB_usePrelaunch";

        private const string MENU_PATH = "Laundry Bear/Force Initialization";

        #region Initialization
        static PlayFromInitializationScene()
        {
            EditorApplication.playModeStateChanged += OnPlayModeChanged;
        }

        [MenuItem(MENU_PATH, validate = true)]
        private static bool ValidateToggle()
        {
            if (!EditorPrefs.HasKey(USE_PRELAUNCH))
            {
                Menu.SetChecked(MENU_PATH, false);
            }
            else
            {
                bool usePreLaunch = EditorPrefs.GetBool(USE_PRELAUNCH);
                Menu.SetChecked(MENU_PATH, usePreLaunch);
            }

            return true;
        }
        #endregion

        #region Menu Toggle
        [MenuItem(MENU_PATH)]
        private static void TogglePreLaunchJump()
        {
            bool usePreLaunch = EditorPrefs.GetBool(USE_PRELAUNCH);
            EditorPrefs.SetBool(USE_PRELAUNCH, !usePreLaunch);
            Menu.SetChecked(MENU_PATH, !usePreLaunch);
        }
        #endregion

        #region Play Mode Changed Handlers
        private static void OnPlayModeChanged(PlayModeStateChange mode)
        {
            if (!EditorPrefs.GetBool(USE_PRELAUNCH, false))
            { return; }

            switch (mode)
            {
                case PlayModeStateChange.EnteredEditMode:
                    OnEnterEditMode();
                    break;
                case PlayModeStateChange.ExitingEditMode:
                    OnExitEditMode();
                    break;
                case PlayModeStateChange.EnteredPlayMode:
                case PlayModeStateChange.ExitingPlayMode:
                default:
                    break;
            }
        }

        private static void OnExitEditMode()
        {
            if (!EditorSceneManager.EnsureUntitledSceneHasBeenSaved(""))
            {
                return;
            }
            if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                return;
            }
            string openScenes = "";
            string activeScenePath = "";
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCount;
            Scene activeScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            EditorBuildSettingsScene initializationScene = EditorBuildSettings.scenes[0];
            for (int sceneIndex = 0; sceneIndex < sceneCount; ++sceneIndex)
            {
                Scene scene = UnityEngine.SceneManagement.SceneManager.GetSceneAt(sceneIndex);
                if (scene.path == initializationScene.path)
                {
                    if (activeScenePath == initializationScene.path)
                    {
                        // run the game as normal
                        openScenes = null;
                        break;
                    }
                    else
                    {
                        // don't include init in the list of scenes to load
                        continue;
                    }
                }
                if (string.IsNullOrEmpty(openScenes))
                {
                    openScenes += scene.path;
                }
                else
                {
                    openScenes += "?" + scene.path;
                }
                if (scene == activeScene)
                {
                    activeScenePath = scene.path;
                }
            }
            SessionState.SetString(SCENES_STRING, openScenes);
            SessionState.SetString(ACTIVE_SCENE_PATH, activeScenePath);
            string path = initializationScene.path;
            EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
        }

        private static void OnEnterEditMode()
        {
            string openScenesString = SessionState.GetString(SCENES_STRING, "");
            string activeScenePath = SessionState.GetString(ACTIVE_SCENE_PATH, "");
            if (string.IsNullOrEmpty(activeScenePath)) { return; }
            string[] openScenesPaths = openScenesString.Split('?');
            for (int sceneIndex = 0; sceneIndex < openScenesPaths.Length; ++sceneIndex)
            {
                OpenSceneMode mode = OpenSceneMode.Additive;
                if (sceneIndex == 0)
                {
                    mode = OpenSceneMode.Single;
                }
                Scene currentScene = EditorSceneManager.OpenScene(openScenesPaths[sceneIndex], mode);
                if (openScenesPaths[sceneIndex] == activeScenePath)
                {
                    UnityEngine.SceneManagement.SceneManager.SetActiveScene(currentScene);
                }
            }
            SessionState.EraseString(SCENES_STRING);
            SessionState.EraseString(ACTIVE_SCENE_PATH);
        }
        #endregion
    }
}
#endif