﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategoryCard : Card
{    
    private void Update()
    {
        if (siblingIndex == -1)
            siblingIndex = transform.GetSiblingIndex();        
    }

    public override void Select()
    {
        if (onSelect != null)
        {
            onSelect.Invoke();
            return;
        }

        if (side == 0)
        {
            Flip(1);
            History.AddUndo(()=> { Flip(0); });
        }

        else
        {
            Flip(0);
            History.AddUndo(() => { Flip(1); });
        }
    }    
}
