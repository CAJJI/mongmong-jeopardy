﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClueCard : Card
{     
    bool isRevealed { get { return side != 0; } }
    bool isDailyDouble;

    private void Update()
    {
        if (siblingIndex == -1)
            siblingIndex = transform.GetSiblingIndex();

        if (!isHidden && isRevealed)
        {
            Vector3 center = new Vector3(Screen.width, Screen.height, 0) / 2;
            
            rectTransform.position = Vector3.Lerp(rectTransform.position, center, zoomSpeed);            
            rectTransform.localScale = Vector3.Lerp(rectTransform.localScale, Vector3.one * expandedScale, zoomSpeed);
        }
    }    

    public override void Select()
    {
        onSelect?.Invoke();
        
        //Daily double
        if (side == 2)
        {            
            Flip(1);
            BuzzInController.SetCanBuzz(true);
        }
       
        //Price side
        else if (side == 0)
        {
            ResetScale();

            transform.SetSiblingIndex(transform.parent.childCount - 1);
            CardController.Instance.SetGridActive(false);
            if (isDailyDouble)
            {                
                Flip(2);
            }
            else
            {
                Flip(1);

                if (Lobby.current.allowEarlyBuzzins)
                {
                    BuzzInController.SetCanBuzz(true);
                }
            }
            History.AddUndo(() =>
            {
                ResetScale();
                transform.SetSiblingIndex(siblingIndex);
                CardController.Instance.SetGridActive(true);
                Flip(0);
                BuzzInController.SetCanBuzz(false);
            });
        }

        //Clue side
        else if (side == 1)
        {
            if (!BuzzInController.Instance.canBuzz)
            {
                BuzzInController.SetCanBuzz(true);
            }

            else
            {
                ResetScale();

                transform.SetSiblingIndex(siblingIndex);
                CardController.Instance.SetGridActive(true);
                Hide(true, false);

                GameController.Instance.CheckCards();
                BuzzInController.SetCanBuzz(false);

                History.AddUndo(() =>
                {
                    ResetScale();
                    transform.SetSiblingIndex(transform.parent.childCount - 1);
                    CardController.Instance.SetGridActive(false);
                    Hide(false, false);
                    //BuzzInController.SetCanBuzz(true);
                });
            }
        }        
    }

    public void SetAsDailyDouble()
    {
        isDailyDouble = true;
    }
}
