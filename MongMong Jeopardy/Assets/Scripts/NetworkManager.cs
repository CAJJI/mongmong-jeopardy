﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;

public class NetworkManager : MonoBehaviourPunCallbacks
{    
    public static int playerLimit = 5;

    public static bool isHost => PhotonNetwork.IsMasterClient;

    static List<RoomInfo> roomList = new List<RoomInfo>();


    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        PhotonNetwork.ConnectUsingSettings();
        //StartCoroutine(TryReconnect());
    }

    public override void OnLeftRoom()
    {
        Scenes.GoToMenu();                
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log(cause);
        Scenes.GoToMenu();
        StartCoroutine(TryReconnect());
    }

    public override void OnConnectedToMaster()
    {        
        Debug.Log("Connected to master");
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        if (GameClient.self.username != "")
        {            
            MenuController.Instance.GoToMainMenu();
        }
        else
        {
            MenuController.Instance.GoToNameEntry();
        }
        Debug.Log("Connected to lobby");
    }
    
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        NetworkManager.roomList = roomList;
    }

    public static bool JoinRoom(string id)
    {
        if (string.IsNullOrEmpty(id) || !RoomExists(id))
            return false;
        return PhotonNetwork.JoinRoom(id);        
    }

    static bool RoomExists(string id)
    {
        bool exists = false;
        foreach (RoomInfo room in roomList)
        {
            if (room.Name == id)
                exists = true;
        }
        return exists;
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("Created room");

        LobbyManager.CreateLobby();
        LobbyMenu.Instance.Open();
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Joined room " + PhotonNetwork.LocalPlayer.ActorNumber);
        LobbyMenu.Instance.Open();

        if (!isHost)
            ServerSend.JoinGame(GameClient.self.username, PhotonNetwork.LocalPlayer.ActorNumber);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {        
        GameClient client = Lobby.GetClient(otherPlayer);
        if (client == null)
        {
            LeaveRoom();
        }
        else
        {
            string username = client.username;
            Lobby.RemoveClient(username);
        }
    }

    public static bool CreateRoom(string id)
    {
        //return PhotonNetwork.CreateRoom(id);
        return PhotonNetwork.CreateRoom(id, new RoomOptions()
        {
            MaxPlayers = System.Convert.ToByte(playerLimit),
            PublishUserId = true,
            PlayerTtl = 0,
        });
    }

    public static void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    IEnumerator TryReconnect()
    {
        int limit = 100;
        while (!PhotonNetwork.IsConnectedAndReady && limit > 0)
        {
            Debug.Log("Try to reconnect " + limit);
            limit--;
            PhotonNetwork.ConnectUsingSettings();
            yield return new WaitForSeconds(2);
        }
        PhotonNetwork.JoinLobby();
    }
}
