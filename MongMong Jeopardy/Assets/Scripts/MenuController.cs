﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuController : Singleton<MenuController>
{    
    //ALLOW host to save and load game incase of disconnect

    //when connecting to lobby open menu
    //input username
    //choose to host or join
    //if host
    //generate 4 letter code? and click host
    //if join
    //input 4 letter code and click join

    //create lobby, wait for players to join
    //when host clicks to start, everyone loads into new scene
    //host loads into question scene
    //players load into button scene

    //if joining an on-going game, go directly to the button scene

    public NameEntryMenu nameEntry;
    public MainMenu mainMenu;
    public LobbyMenu lobbyMenu;

    public TextMeshProUGUI loadingText;

    private void Awake()
    {
        loadingText.gameObject.SetActive(true);
    }

    public void GoToNameEntry()
    {
        CloseAll();
        nameEntry.Open();
    }

    public void GoToMainMenu()
    {
        CloseAll();
        mainMenu.Open();
    }

    public void GoToLobbyMenu()
    {
        CloseAll();
        lobbyMenu.Open();
    }

    public void CloseAll()
    {
        loadingText.gameObject.SetActive(false);
        nameEntry.Close();
        mainMenu.Close();
        lobbyMenu.Close();
    }      

    public void Quit()
    {
        Application.Quit();
    }
}
