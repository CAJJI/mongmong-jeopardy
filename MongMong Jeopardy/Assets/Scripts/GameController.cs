﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance { get { if (_instance == null) _instance = FindObjectOfType<T>(); return _instance; } }
    static T _instance;

    public static bool Exists { get { return Instance != null; } }
}

[System.Serializable]
public class RoundInfo
{
    public Sprite titleSprite;
    public int roundIndex = 1;
    public string sheetName;
}

public class GameController : Singleton<GameController>
{
    string fileExtension = ".tsv";
    string sheetFolderPath = "Assets/";        

    public RoundInfo[] rounds;
    int roundIndex;

    private void Start()
    {        
        SetupRound(rounds[0], true);
    }

    public void SetupRound(RoundInfo info, bool startInstant = false)
    {
        History.ClearHistory();
        CardController.Instance.SetGridActive(true);

        roundIndex = info.roundIndex;

        string path = sheetFolderPath;


        StreamReader sr = new StreamReader(path + info.sheetName + fileExtension);
        TextAsset cardSheet = new TextAsset(sr.ReadToEnd());
        //Debug.Log(info.sheetName + fileExtension);
        //TextAsset cardSheet = Resources.Load<TextAsset>(info.sheetName + fileExtension);

        Debug.Log(cardSheet);

        if (startInstant)
        {
            CardController.Instance.SetupSheet(cardSheet, info.roundIndex);
            TitleScreen.Instance.FadeOut(() =>
            {

            });
        }

        else
        {
            TitleScreen.Instance.FadeIn(info.titleSprite, () =>
            {
                TitleScreen.Instance.SetOnSelect(() =>
                {
                    CardController.Instance.SetupSheet(cardSheet, info.roundIndex);
                    TitleScreen.Instance.FadeOut(() =>
                    {

                    });
                }, true);
            });
        }
    }

    private void Update()
    {
        Inputs();
    }

    void Inputs()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            History.Undo();
        }
    }

    public void CheckCards()
    {
        bool canContinue = true;
        foreach(Card card in CardController.Instance.clueCards)
        {
            if (!card.isHidden)
            {
                canContinue = false;
                break;
            }
        }

        if (!canContinue)
            return;

        if (roundIndex < rounds.Length)
        {
            SetupRound(rounds[roundIndex]);
        }
        else
        {
            EndGame();
            //roundIndex = 0;
            //SetupRound(rounds[roundIndex]);            
        }
    }

    void EndGame()
    {
        TitleScreen.Instance.FadeIn(rounds[0].titleSprite, () =>
        {
            TitleScreen.Instance.SetOnSelect(() =>
            {
                ServerSend.EndGame();                
            }, false);
        });
    }
}
