﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameEntryMenu : MenuBase<NameEntryMenu>
{
    public void SetUsername(string name)
    {
        GameClient.self.username = name;
    }

    public void Continue()
    {
        if (GameClient.self.username.Length > 0)
            MenuController.Instance.GoToMainMenu();
    }
}
