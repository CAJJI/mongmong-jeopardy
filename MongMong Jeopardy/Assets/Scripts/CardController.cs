﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class History
{
    public static List<Action> history = new List<Action>();

    public static void ClearHistory()
    {
        history.Clear();
    }

    public static void AddUndo(Action undo)
    {
        history.Add(undo);
    }

    public static void Undo()
    {
        if (history.Count == 0)
            return;

        int index = history.Count - 1;
        history[index].Invoke();
        history.RemoveAt(index);

        Debug.Log(history.Count);
    }

}

public class CardController : Singleton<CardController>
{
    public GameObject cardContainer;
    public GridLayoutGroup grid;

    public List<Card> cards = new List<Card>();
    public List<Card> clueCards = new List<Card>();

    public Card categoryCardPrefab;
    public Card clueCardPrefab;    

    public void SetupSheet(TextAsset sheet, int round)
    {
        ClearCards();
        ParseSheet(sheet, round);
    }

    void ClearCards()
    {
        for(int i = 0; i < cards.Count; i++)
        {
            Destroy(cards[i].gameObject);
        }

        cards.Clear();
        clueCards.Clear();
    }

    public void ParseSheet(TextAsset sheet, int round)
    {
        string text = sheet.text;

        string[] rows = text.Split('\n');
        int maxColumns = rows[0].Split('\t').Length;

        List<int> dailyDoubles = new List<int>();

        if (round < 3)
        {
            for (int i = 0; i < round; i++)
            {
                int attempts = 100;
                bool addIndex = false;
                int index = 0;

                while (attempts > 0 && !addIndex)
                {                    
                    int randomRow = UnityEngine.Random.Range(3, rows.Length);
                    int randomColumn = UnityEngine.Random.Range(0, maxColumns);
                    index = (randomRow * maxColumns) + randomColumn;

                    if (!dailyDoubles.Contains(index))
                        addIndex = true;

                    attempts--;
                }                        

                dailyDoubles.Add(index);
                Debug.Log(index);
            }
        }

        for (int i = 0; i < rows.Length; i++)
        {
            bool isCategory = i == 0;

            string[] columns = rows[i].Split('\t');

            for (int y = 0; y < columns.Length; y++)
            {
                string cellText = columns[y];

                Card prefab = isCategory ? categoryCardPrefab : clueCardPrefab;
                Card card = Instantiate(prefab.gameObject, cardContainer.transform).GetComponent<Card>();
                
                card.Flip(0);
                card.ResetScale();

                if (isCategory)
                {                    
                    card.SetText(1, cellText);

                    if (round == 3)
                    {
                        card.Flip(1);
                        card.Enlarge();
                        card.onSelect = () => {
                            card.Hide(true, false);
                            if (Lobby.current.allowEarlyBuzzins)
                            {
                                BuzzInController.SetCanBuzz(true);
                            }
                        };
                    }
                }
                else
                {
                    int price = (i * 100) * round;
                    string front = "$" + price;                    
                    card.SetText(0, front);
                    card.SetText(1, cellText);

                    if (round < 3)
                    {
                        if (dailyDoubles.Contains((i * maxColumns) + y))
                        {
                            (card as ClueCard).SetAsDailyDouble();
                        }
                    }
                    else
                    {
                        card.Enlarge();
                        card.transform.SetSiblingIndex(0);
                        card.Flip(1);
                        card.onSelect = () => {
                            if (BuzzInController.Instance.canBuzz)                            
                            {
                                card.Hide(true, true);
                            }
                        };
                    }

                    clueCards.Add(card);
                }
                
                cards.Add(card);
            }
        }
    }    

    public void SetGridActive(bool state)
    {        
        grid.enabled = state;
    }
}
