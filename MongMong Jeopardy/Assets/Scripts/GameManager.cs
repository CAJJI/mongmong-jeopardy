﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        Scenes.GoToMenu();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            NetworkManager.LeaveRoom();            
        }
    }
}
