﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameClient 
{
    public static GameClient self { get { return GetSelf(); } }    
    static GameClient _self;

    public static int localPlayerId => PhotonNetwork.LocalPlayer.ActorNumber;

    public string username;
    public int playerId;

    static GameClient GetSelf()
    {
        if (_self == null)
        {
            _self = new GameClient("", -1);
        }
        return _self;
    }    

    public GameClient(string username, int playerId)
    {
        this.username = username;
        this.playerId = playerId;
    }
}
