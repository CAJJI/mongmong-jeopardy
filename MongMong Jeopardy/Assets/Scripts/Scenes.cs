﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenes
{
    public static string MENU = "Menu";
    public static string GAME = "Game";
    public static string BUZZER = "Buzzer";

    public static string activeScene => SceneManager.GetActiveScene().name;

    public static void Load(string name)
    {
        SceneManager.LoadScene(name);
    }

    public static void GoToMenu()
    {
        Load(MENU);
    }

    public static void GoToGame()
    {
        Load(GAME);
    }

    public static void GoToBuzzer()
    {
        Load(BUZZER);
    }
}
