﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lobby
{
    public static Lobby current => LobbyManager.lobby;

    public bool inGame;

    public bool allowEarlyBuzzins;

    public static Dictionary<string, GameClient> clients { get { return LobbyManager.lobby._clients; } set { LobbyManager.lobby._clients = value; } }
    public Dictionary<string, GameClient> _clients = new Dictionary<string, GameClient>();

    public static void AddClient(GameClient client)
    {
        clients.Add(client.username, client);

        if (LobbyMenu.Exists && LobbyMenu.Instance.isOpen)
        {
            LobbyMenu.Instance.UpdateLobby();
        }
    }

    public static void AddClient(string username, int playerId)
    {
        GameClient client = new GameClient(username, playerId);
        AddClient(client);        
    }

    public static void RemoveClient(string username)
    {
        clients.Remove(username);

        if (LobbyMenu.Exists && LobbyMenu.Instance.isOpen)
        {
            LobbyMenu.Instance.UpdateLobby();
        }
    }

    public static string[] GetUsernames()
    {
        List<string> usernames = new List<string>();
        foreach(string username in clients.Keys)
        {
            usernames.Add(username);
        }
        return usernames.ToArray();
    }

    public static int[] GetPlayerIds()
    {
        List<int> playerIds = new List<int>();
        foreach (GameClient client in clients.Values)
        {
            Debug.Log(client.playerId);
            playerIds.Add(client.playerId);
        }
        return playerIds.ToArray();
    }

    public static GameClient GetClient(Photon.Realtime.Player player)
    {
        foreach(GameClient client in clients.Values)
        {
            if (client.playerId == player.ActorNumber)
                return client;
        }
        return null;
    }
}

public class LobbyManager : MonoBehaviour
{
    public static Lobby lobby;

    public static void CreateLobby()
    {
        lobby = new Lobby();
    }

    //public void JoinLobby(Lobby lobby)
    //{
    //    LobbyManager.lobby = lobby;
    //}
}
