﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class CardSide
{
    public Image image;
    public TextMeshProUGUI text;
}

public class Card : MonoBehaviour
{
    [HideInInspector]
    public int siblingIndex = -1;

    public float expandedScale = 6;
    public float zoomSpeed = 0.2f;

    public float defaultScale = 0.8f;

    public GameObject container;

    //public Image frontImage;
    //public Image backImage;
    //public TextMeshProUGUI frontText;
    //public TextMeshProUGUI backText;

    //public bool isFlipped;
    public bool isHidden;

    public List<CardSide> sides = new List<CardSide>();

    public int side;

    [HideInInspector]
    public RectTransform rectTransform;

    public Action onSelect;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();        
    }

    public void Enlarge()
    {
        CardController.Instance.SetGridActive(false);
        Vector3 center = new Vector3(Screen.width, Screen.height, 0) / 2;
        transform.position = center;
        transform.localScale = Vector3.one * expandedScale;
    }

    public void ResetScale()
    {
        transform.localScale = Vector3.one * defaultScale;
    }

    //public virtual void Initialize(string fText, string bText)
    //{
    //    frontText.text = fText;
    //    backText.text = bText;

    //    Flip(true);
    //}

    public void SetText(int side, string text)
    {
        sides[side].text.text = text;
    }

    //public virtual void Flip()
    //{
    //    Flip(isFlipped);
    //}

    public virtual void Flip(int side)
    {
        Clear();
        sides[side].text.gameObject.SetActive(true);
        sides[side].image.gameObject.SetActive(true);
        this.side = side;
    }

    public virtual void Clear()
    {
        foreach(CardSide side in sides)
        {
            side.text.gameObject.SetActive(false);
            side.image.gameObject.SetActive(false);
        }
    }

    public void Hide(bool state, bool entirely)
    {
        if (entirely)
            gameObject.SetActive(!state);
        else 
            container.SetActive(!state);

        isHidden = state;
    }

    //public virtual void Flip(bool front)
    //{
    //    frontImage.gameObject.SetActive(front);
    //    frontText.gameObject.SetActive(front);

    //    backImage.gameObject.SetActive(!front);
    //    backText.gameObject.SetActive(!front);

    //    isFlipped = !front;
    //}        

    public virtual void Select()
    {
        
    }    
}
