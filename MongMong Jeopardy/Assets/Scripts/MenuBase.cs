﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBase<T> : Singleton<T> where T : MonoBehaviour
{    
    public GameObject container;

    public bool isOpen;    

    public virtual void Open()
    {
        Open(true);        
    }

    public virtual void Close()
    {
        Open(false);        
    }

    public virtual void Open(bool state)
    {
        container.SetActive(state);
        isOpen = state;
    }
}
