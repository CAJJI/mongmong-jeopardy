﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleScreen : Singleton<TitleScreen>
{
    public Image image;
    public float fadeSpeed = 0.1f;
    public Action fadeAction;

    public Action onSelect;
    bool clearAfterSelect;

    private void Update()
    {        
        fadeAction?.Invoke();        
    }

    public void Select()
    {
        onSelect?.Invoke();
        if (clearAfterSelect)
            onSelect = null;
    }

    public void FadeIn(Action onEnd)
    {
        FadeIn(image.sprite, onEnd);
    }

    public void FadeIn(Sprite sprite, Action onEnd)
    {
        FadeIn(sprite, fadeSpeed, onEnd);
    }

    public void FadeIn(Sprite sprite, float speed, Action onEnd)
    {
        image.enabled = true;
        image.sprite = sprite;
        Color currentColor = image.color;
        currentColor.a = 0;

        fadeAction = () =>
        {
            Color color = image.color;
            color.a += Time.deltaTime * speed;
            image.color = color;

            if (color.a >= 1)
            {                
                fadeAction = null;
                onEnd?.Invoke();
            }
        };
    }

    public void FadeOut(Action onEnd)
    {
        FadeOut(fadeSpeed, onEnd);
    }

    public void FadeOut(float speed, Action onEnd)
    {
        image.enabled = true;
        Color currentColor = image.color;
        currentColor.a = 1;

        fadeAction = () =>
        {
            Color color = image.color;
            color.a -= Time.deltaTime * speed;
            image.color = color;

            if (color.a <= 0)
            {
                image.enabled = false;
                fadeAction = null;

                onEnd?.Invoke();
            }
        };
    }

    public void SetOnSelect(Action action, bool clearOnSelect)
    {
        onSelect = action;
        clearAfterSelect = clearOnSelect;
    }
}
