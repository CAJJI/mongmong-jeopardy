﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BuzzInController : Singleton<BuzzInController>
{
    public TextMeshProUGUI usernameText;
    public RectTransform topPosition;
    public RectTransform bottomPosition;

    public AudioSource audioSource;

    public float moveSpeed = 0.1f;
    
    public bool active;
    public bool canBuzz;

    public GameObject continueButton;

    List<int> buzzedInPlayers = new List<int>();

    //when card is opened, set canBuzz to true
    //when user buzz's in, disable canBuzz
    //when card is closed, set inactive and disable canBuzz

    private void Update()
    {
        Vector3 target = bottomPosition.position;

        if (active)
        {
            target = topPosition.position;            
        }

        usernameText.rectTransform.position = Vector3.Lerp(usernameText.rectTransform.position, target, moveSpeed);
    }

    public void Continue()
    {
        active = false;
        canBuzz = true;
        continueButton.SetActive(false);
    }

    public void BuzzIn(string username, int playerId)
    {
        Debug.Log("Buzz in: " + username);
        if (canBuzz)
        {
            if (!buzzedInPlayers.Contains(playerId))
            {
                audioSource.Play();
                usernameText.text = username;
                active = true;
                canBuzz = false;
                continueButton.SetActive(true);

                buzzedInPlayers.Add(playerId);
            }
        }
    }

    public static void SetCanBuzz(bool state)
    {
        Instance.canBuzz = state;
        Instance.active = false;
        Instance.continueButton.SetActive(false);

        Instance.buzzedInPlayers.Clear();
    }
}
