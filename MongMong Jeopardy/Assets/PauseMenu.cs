﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PauseMenu : MenuBase<PauseMenu>
{
    public TextMeshProUGUI roomIdText;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Open(!isOpen);
        }
    }

    public override void Open(bool state)
    {
        base.Open(state);
        roomIdText.text = "Room Id: " + Photon.Pun.PhotonNetwork.CurrentRoom.Name;
    }

    public void Resume()
    {
        Close();
    }

    public void Quit()
    {
        NetworkManager.LeaveRoom();
    }
}
